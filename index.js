import React, { Component, Fragment } from 'react';
import { NavLink, Link } from 'react-router-dom';
import BasicState from '../../layouts/BasicState';
import { __ } from '../../layouts/utilities/i18n';

class HTMLState extends BasicState {
    getRoute = route => this.props.route
}
export default HTMLState;
